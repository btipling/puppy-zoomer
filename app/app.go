package app

import (
	"bytes"
	"fmt"
	"gitlab.com/btipling/img-transfer/zoomer"
	"image"
	"image/jpeg"
	_ "image/jpeg"
	_ "image/png"
	"syscall/js"
)
type App struct {
	zoom  js.Func
	done    chan struct{}
}

type Zoomer struct {
}

func New() *App {
	return &App{}
}

func (a *App) Setup() {
	fmt.Println("Setting up Zoom app")
	a.zoom = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		mainImgBytes := make([]byte, args[0].Length())
		js.CopyBytesToGo(mainImgBytes, args[0])
		z, err := zoomer.New(mainImgBytes, args[1].Int(), args[2].Int())
		if err != nil {
			fmt.Printf("Error decoding main image in zoomer: %s\n", err.Error())
			return nil
		}
		z.GenerateZoomImage()
		a.updateZoomWindow(z.Result())
		return nil
	})
	js.Global().Set("zoom", a.zoom)
	<-a.done
}

func (a *App) updateZoomWindow(zoomImg *image.RGBA) {
	var buf bytes.Buffer
	err := jpeg.Encode(&buf, zoomImg, nil)
	if err != nil {
		fmt.Println("Error encoding jpeg")
		return
	}
	b := buf.Bytes()
	fmt.Printf("Updating zoom window with bytes %d \n", len(b))
	dst := js.Global().Get("Uint8Array").New(len(b))
	js.CopyBytesToJS(dst, b)
	js.Global().Call("updateZoomWindow", dst)
}
