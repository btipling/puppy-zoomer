package zoomer

import (
	"bytes"
	"fmt"
	"image"
	"image/draw"
)

const (
	mainWinDim = 360
	zoomWinDim = 50
)


type Zoomer struct {
	mainImage image.Image
	zoomWindow *image.RGBA
	zoomTopX int
	zoomTopY int
}

func New(imgBytes []byte, zoomTopX int, zoomTopY int) (*Zoomer, error) {
	fmt.Printf("Zoom to x: %d, y: %d in image with %d bytes.\n", zoomTopX, zoomTopY, len(imgBytes))
	mainImgReader := bytes.NewReader(imgBytes)
	mainImage, _, err := image.Decode(mainImgReader)
	if err != nil {
		return nil, err
	}
	zoomWindow := image.NewRGBA(image.Rect(0, 0, mainWinDim, mainWinDim))
	return &Zoomer {
		mainImage: mainImage,
		zoomWindow: zoomWindow,
		zoomTopX: zoomTopX,
		zoomTopY: zoomTopY,
	}, nil
}

func (z *Zoomer) GenerateZoomImage() {
	zoomWindowXIndex := 0
	zoomSize := mainWinDim/zoomWinDim
	for x := 0; x < mainWinDim; x += zoomSize {
		zoomWindowYIndex := 0
		for y := 0; y < mainWinDim; y += zoomSize {
			clr := z.mainImage.At(z.zoomTopX + zoomWindowXIndex, z.zoomTopY + zoomWindowYIndex)
			bounds := image.Rectangle{
				Min: image.Point{X: x, Y: y},
				Max: image.Point{X: x + zoomSize, Y: y + zoomSize},
			}
			draw.Draw(z.zoomWindow, bounds, &image.Uniform{clr}, image.ZP, draw.Src)
			zoomWindowYIndex += 1
		}
		zoomWindowXIndex += 1
	}
}

func (z *Zoomer) Result() *image.RGBA {
	return z.zoomWindow
}