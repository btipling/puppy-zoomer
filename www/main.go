package main

import (
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"log"
	"net/http"
)

const (
	INDEX_HTML = "./html/index.html"
	STATIC = "./static"
	IMAGE = "./data/doggo.jpg"
)

func serveImage(c *gin.Context) {
	data, err := ioutil.ReadFile(IMAGE)
	if err != nil {
		c.Status(http.StatusInternalServerError)
		return
	}
	contentType := "image/jpeg"

	c.Header("Content-Disposition", `attachment; filename="doggo.png"`)
	c.Data(http.StatusOK, contentType, data)
}

func main() {
	r := gin.Default()
	r.Static("/static", STATIC)
	r.StaticFile("/", INDEX_HTML)
	r.GET("/doggo", serveImage)
	err := r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
}