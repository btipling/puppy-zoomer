
const imagePath = '/doggo';

const maxDim = 360;
const zoomWinDim = 50;

const fetchImage = async () => {
    const resp = await fetch(imagePath);
    const imgData = await resp.blob();
    console.log('imgData', imgData);
    return imgData;
}

const setImage = async () => {
    const imgData = await fetchImage();
    const imgDataBuf = await imgData.arrayBuffer()
    const view = new Uint8Array(imgDataBuf);
    console.log('view?', view.length);
    const container$ = document.getElementById('mainImgContainer');
    const img$ = new Image();
    img$.src = window.URL.createObjectURL(imgData);
    img$.id = 'mainImg';
    img$.onmousedown = onMainImageClick(view);
    img$.draggable = false;
    container$.append(img$);
}

const xyFromOffset = (offsetX, offsetY) => {
    let x = offsetX;
    let y = offsetY;

    x -= zoomWinDim/2;
    y -= zoomWinDim/2;
    if (x < 0) {
        x = 0;
    }
    if (y < 0) {
        y = 0;
    }
    if (x + zoomWinDim > maxDim) {
        x = maxDim - zoomWinDim;
    }
    if (y + zoomWinDim > maxDim) {
        y = maxDim - zoomWinDim;
    }
    return {x, y}
}

const positionZoomWin = (x, y) => {
    const zoomWin$ = document.getElementById("zoomWin");
    zoomWin$.style.visibility = 'visible';
    zoomWin$.style.left = x;
    zoomWin$.style.top = y;
}

const onMainImageClick = (imgData) => async (event) => {
    console.log('img data length?', imgData.length);
    const { offsetX, offsetY } = event;
    console.log('x', offsetX, 'y', offsetY);
    const { x, y } = xyFromOffset(offsetX, offsetY);
    positionZoomWin(x, y);
    zoom(imgData, x, y);
}

function updateZoomWindow(buf) {
    console.log('updating zoom window!')
    const zoomViewer$ = document.getElementById('zoomViewer');
    const blob = new Blob([buf], {'type': 'jpeg'});
    zoomViewer$.src = URL.createObjectURL(blob);
}

function main() {
    console.log('Hello World!');

    const go = new Go();
    WebAssembly.instantiateStreaming(fetch("static/main.wasm"), go.importObject).then((result) => {
        go.run(result.instance);
    });
    setImage();
}

main();

