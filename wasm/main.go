package main

import (
	"fmt"
	"gitlab.com/btipling/img-transfer/app"
)

func main() {
	fmt.Println("Hello, WebAssembly!")
	a := app.New()
	a.Setup()
}
